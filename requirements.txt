# core packages needed to run the game
jupyterlab==2.2.8
pygame==1.9.6
numpy==1.19.2
networkx~=2.5
