# self-solving snake

Game of snake + 2 AIs to make it solve itself.

One is based on ANN and trained using genetic algorithm.  
The other finds paths in the graph made by grid to find a way to get the food and avoid collisions.

![picture](picture.png)

You can run the learning algorithm for the first one and watch results in *runner.ipynb*.
Results are saved in *new_cave* files.  
You can notice some learnt habits after a couple of generations run but
the learning algorithm produces snakes that could do much better.

The PathFinder brain is going to use different strategy and reach higher scores but currently is not being developed.
