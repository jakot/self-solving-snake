###############################################################################
#
#  ▄▄▄▄    ██▀███   ▄▄▄       ██▓ ███▄    █
# ▓█████▄ ▓██ ▒ ██▒▒████▄    ▓██▒ ██ ▀█   █
# ▒██▒ ▄██▓██ ░▄█ ▒▒██  ▀█▄  ▒██▒▓██  ▀█ ██▒
# ▒██░█▀  ▒██▀▀█▄  ░██▄▄▄▄██ ░██░▓██▒  ▐▌██▒
# ░▓█  ▀█▓░██▓ ▒██▒ ▓█   ▓██▒░██░▒██░   ▓██░
# ░▒▓███▀▒░ ▒▓ ░▒▓░ ▒▒   ▓▒█░░▓  ░ ▒░   ▒ ▒
# ▒░▒   ░   ░▒ ░ ▒░  ▒   ▒▒ ░ ▒ ░░ ░░   ░ ▒░
#  ░    ░   ░░   ░   ░   ▒    ▒ ░   ░   ░ ░
#  ░         ░           ░  ░ ░           ░
#       ░
###############################################################################
from collections import deque

import networkx as nx
import numpy as np

from .game import Direction


class Brain:
    """Base for other Brains."""

    def possess(self, snake):
        pass

    def take_a_look(self):
        """Know the surrounding."""
        pass

    def evaluate(self):
        """Make a decision."""
        pass


def af(x):
    return np.where(x > 0, x, 0)


class NNBrain(Brain):
    """AI class based on ann for snake to play alone.

    Uses 2-layer neural network to evaluate snake's behaviour
    depending on 8-way look and food location as inputs.
    """

    def __init__(self, empty=False, layer1=8, layer2=8):
        """Initialize the brain with random parameters.

        layer1, layer2: numbers of units in every layer.
        """

        self.snake = None
        self.units = []
        self.biases = []

        # Initialize empty array for crossover
        if empty:
            self.units.append(np.empty((layer1, 12)))
            self.units.append(np.empty((layer2, layer1)))
            self.units.append(np.empty((4, layer2)))

            self.biases.append(np.empty(layer1))
            self.biases.append(np.empty(layer2))
            self.biases.append(np.empty(4))
        else:
            self.units.append(np.random.normal(size=(layer1, 12)))
            self.units.append(np.random.normal(size=(layer2, layer1)))
            self.units.append(np.random.normal(size=(4, layer2)))

            self.biases.append(np.random.normal(size=layer1))
            self.biases.append(np.random.normal(size=layer2))
            self.biases.append(np.random.normal(size=4))

    def possess(self, snake):
        """Let the brain control the snake."""

        self.snake = snake

    def take_a_look(self):
        """Look in every direction and make an overview.

        The brain should look in all 4 directions and diagonals as well, producing 8 numbers.
        The array contains information about food location after looking for it in 4 basic directions.
        Total: 12 numbers.

        returns: 12-elem array.
        """

        h, w = self.snake.env.h, self.snake.env.w
        field = np.zeros((h, w), dtype=bool)
        for piece in self.snake.body[1:]:
            field[piece[0], piece[1]] = True

        res = np.zeros(12)
        head_pos = tuple(self.snake.body[0])

        # RIGHT-UP-LEFT-DOWN
        i = 1
        while head_pos[1] + i < w and field[head_pos[0], head_pos[1] + i] is False:
            i += 1
        res[0] = i

        i = 1
        while head_pos[0] - i >= 0 and field[head_pos[0] - i, head_pos[1]] is False:
            i += 1
        res[1] = i

        i = 1
        while head_pos[1] - i >= 0 and field[head_pos[0], head_pos[1] - i] is False:
            i += 1
        res[2] = i

        i = 1
        while head_pos[0] + i < h and field[head_pos[0] + i, head_pos[1]] is False:
            i += 1
        res[3] = i

        # diagonally UR, UL, DL, DR
        i = 1
        while (head_pos[0] - i >= 0 and head_pos[1] + i < w and
               field[head_pos[0] - i, head_pos[1] + i] is False):
            i += 1
        res[5] = i * 1.41

        i = 1
        while (head_pos[0] - i >= 0 and head_pos[1] - i >= 0 and
               field[head_pos[0] - i, head_pos[1] - i] is False):
            i += 1
        res[6] = i * 1.41

        i = 1
        while (head_pos[0] + i < h and head_pos[1] - i >= 0 and
               field[head_pos[0] + i, head_pos[1] - i] is False):
            i += 1
        res[7] = i * 1.41

        i = 1
        while (head_pos[0] + i < h and head_pos[1] + i < w and
               field[head_pos[0] + i, head_pos[1] + i] is False):
            i += 1
        res[4] = i * 1.41

        food_pos = tuple(self.snake.env.food)
        if food_pos[0] == head_pos[0]:
            dist = food_pos[1] - head_pos[1]
            if food_pos[1] > head_pos[1] and dist < res[0]:  # 2nd condition: walls vision
                res[8] = 1
            elif -dist < res[2]:
                res[10] = 1
        elif food_pos[1] == head_pos[1]:
            dist = food_pos[0] - head_pos[0]
            if food_pos[0] < head_pos[0] and -dist < res[1]:
                res[9] = 1
            elif dist < res[3]:
                res[11] = 1

        res[:8] -= 1  # to produce '0' when head near an obstacle
        res[[1, 3]] /= h
        res[[0, 2]] /= w
        res[[4, 5, 6, 7]] /= min(h, w)

        return res

    def evaluate(self):
        """Evaluate brain's decision and change snake's direction."""

        situation = self.take_a_look()
        a1 = af(self.units[0].dot(situation) + self.biases[0])
        a2 = af(self.units[1].dot(a1) + self.biases[1])
        a3 = af(self.units[2].dot(a2) + self.biases[2])

        direction = Direction(np.argmax(a3) + 1)

        if direction == Direction.RIGHT and self.snake.direction != Direction.LEFT:
            self.snake.direction = Direction.RIGHT
        elif direction == Direction.UP and self.snake.direction != Direction.DOWN:
            self.snake.direction = Direction.UP
        elif direction == Direction.LEFT and self.snake.direction != Direction.RIGHT:
            self.snake.direction = Direction.LEFT
        elif direction == Direction.DOWN and self.snake.direction != Direction.UP:
            self.snake.direction = Direction.DOWN

    def mutate(self, f=0.25):
        """Mutate the brain by changing some of its parameters randomly.

        f: mean fraction of parameters in every layer to change.
        """

        for layer in self.units:
            shape = layer.shape
            mask = np.random.random_sample(shape)
            layer[:, :] = np.where(mask < f, np.random.random_sample(shape), layer)

        for layer in self.biases:
            shape = layer.shape
            mask = np.random.random_sample(shape)
            layer[:] = np.where(mask < f, np.random.random_sample(shape), layer)

    def crossover(self, other):
        """Produce an offspring of two brain objects, mixing their parameters with random weights."""

        child = NNBrain(empty=True)

        for self_layer, other_layer, child_layer in zip(self.units, other.units, child.units):
            shape = self_layer.shape
            weights = np.random.random_sample(shape)
            child_layer[:, :] = self_layer * weights + other_layer * (1.0 - weights)

        for self_layer, other_layer, child_layer in zip(self.biases, other.biases, child.biases):
            shape = self_layer.shape
            weights = np.random.random_sample(shape)
            child_layer[:] = self_layer * weights + other_layer * (1.0 - weights)

        return child


class PathFinder(Brain):
    """Brain using graph paths finding algorithm.

    A snake supplied with the brain follows shortest path to the food assuming it can reach it's tail.
    If there's no such path, it tries to follow it's tail until it finds a way to reach the food.
    """

    def __init__(self):
        """Initialize the brain and prepare the graph to find paths on."""

        self.snake = None
        self.G = nx.Graph()
        self.path = deque()

    def possess(self, snake):
        """Let the brain control the snake."""

        self.snake = snake
        self.build_graph()

    def build_graph(self):
        """Add nodes and edges to the graph but exclude the ones containing pieces of snake.

        The function lets snake's head and tail stay for easy path finding.
        """

        self.G.clear()
        for n in range(self.snake.env.h - 1):
            for m in range(self.snake.env.w - 1):
                self.G.add_edge((n, m), (n + 1, m))
                self.G.add_edge((n, m), (n, m + 1))
        for i in range(self.snake.env.w - 1):
            self.G.add_edge((self.snake.env.h - 1, i), (self.snake.env.h - 1, i + 1))
        for i in range(self.snake.env.h - 1):
            self.G.add_edge((i, self.snake.env.w - 1), (i + 1, self.snake.env.w - 1))

        self.G.remove_nodes_from(tuple(piece) for piece in self.snake.body[1:-1])

    def take_a_look(self):
        """Find a path to follow.

        If there is a path to the tail that enables snake to reach food before it is chosen.
        If it's not the brain tries to go one step into tail then it is supposed to take a look again.
        """

        head, tail = tuple(self.snake.body[0]), tuple(self.snake.body[-1])
        food_pos = tuple(self.snake.env.food)

        # TODO: over here, not only go for food but also avoid collisions
        self.build_graph()
        self.path.extend(list(nx.algorithms.shortest_path(self.G, head, food_pos))[1:])

    def evaluate(self):
        """Make or read made path and make a move following the path.

        The function also updates graph's state, i.e. it removes and adds proper nodes and edges
        when the snake makes a move.
        """

        head = tuple(self.snake.body[0])

        if not self.path:
            self.take_a_look()

        next_node = self.path.popleft()
        if head[0] < next_node[0]:
            self.snake.direction = Direction.DOWN
        elif head[0] > next_node[0]:
            self.snake.direction = Direction.UP
        elif head[1] < next_node[1]:
            self.snake.direction = Direction.RIGHT
        elif head[1] > next_node[1]:
            self.snake.direction = Direction.LEFT
