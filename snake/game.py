###############################################################################
#
# ███╗   ███╗ █████╗ ██╗███╗   ██╗    ███████╗████████╗██╗   ██╗███████╗███████╗
# ████╗ ████║██╔══██╗██║████╗  ██║    ██╔════╝╚══██╔══╝██║   ██║██╔════╝██╔════╝
# ██╔████╔██║███████║██║██╔██╗ ██║    ███████╗   ██║   ██║   ██║█████╗  █████╗
# ██║╚██╔╝██║██╔══██║██║██║╚██╗██║    ╚════██║   ██║   ██║   ██║██╔══╝  ██╔══╝
# ██║ ╚═╝ ██║██║  ██║██║██║ ╚████║    ███████║   ██║   ╚██████╔╝██║     ██║
# ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝    ╚══════╝   ╚═╝    ╚═════╝ ╚═╝     ╚═╝
#
###############################################################################
from enum import Enum, auto
import random


class Direction(Enum):
    """Four directions in which a snake can move."""

    RIGHT = auto()
    UP = auto()
    LEFT = auto()
    DOWN = auto()


class Grid:
    """A snake's habitat."""

    def __init__(self, h, w=None, roll=False):
        """Initialize the field.

        H, W: grid's sizes
        roll: whether borders should teleport
        """
        if w is None:
            w = h
        self.h = h
        self.w = w
        self.snake = None
        self.roll = roll
        self.food = None
        self.put_food()

    def step(self):
        """Move everything one step."""

        assert self.snake, 'No snake on the field to make a move.'
        self.snake.move()

    def food_eaten(self):
        """Remove eaten food from the grid."""

        self.food = None
        self.put_food()

    def put_food(self):
        """Put food on a random free place."""

        field = {(i, j) for i in range(self.h) for j in range(self.w)}

        if self.snake:
            field -= set(tuple(piece) for piece in self.snake.body)

        place_y, place_x = random.choice(list(field))
        self.food = [place_y, place_x]

    def put_snake(self, snake):
        """Put a snake on the grid."""

        self.snake = snake


class Snake:
    """The playable animal."""

    def __init__(self, grid, cells=4, brain=None):
        """Init the snake with given length."""

        self.env = grid
        grid.put_snake(self)
        self.brain = brain

        self.cells = cells
        self.direction = Direction.RIGHT
        self.body = []
        assert cells < self.env.w // 2, 'Snake too long to start a game...'
        for i in range(cells):
            self.body.append([self.env.h // 2, self.env.w // 2 - i])

        if brain:
            brain.possess(self)

    def grow(self):
        """Add one cell to snake's body."""

        self.cells += 1
        self.body.append(self.body[-1].copy())

    def on_food(self):
        """Check whether the snake's head reached food."""

        return self.body[0] == self.env.food

    def eat_food(self):
        """Feed the snake."""

        self.grow()
        self.env.food_eaten()

    def move(self):
        """Move whole snake on the field."""

        # Begin at the tail and update all cells' positions.
        for i in range(self.cells - 1)[::-1]:
            self.body[i + 1] = self.body[i].copy()

        if self.direction == Direction.RIGHT:
            self.body[0][1] += 1
        if self.direction == Direction.UP:
            self.body[0][0] -= 1
        if self.direction == Direction.LEFT:
            self.body[0][1] -= 1
        if self.direction == Direction.DOWN:
            self.body[0][0] += 1

        if self.env.roll:
            for piece in self.body:
                piece[0] %= self.env.h
                piece[1] %= self.env.w

        if self.on_food():
            self.eat_food()

    def in_field(self):
        """Check whether the snake crossed the border."""

        # Only head needed to be checked since every other piece just follows it.
        return 0 <= self.body[0][0] < self.env.h and 0 <= self.body[0][1] < self.env.w

    def self_destruction(self):
        """Check whether the snake is trying to eat itself."""

        head = self.body[0]
        if any(head == piece for piece in self.body[1:]):
            return True
        return False

    def use_brain(self):
        """Use brain to figure out situation and decide on the next step."""

        self.brain.evaluate()
