import random
import shelve
import time

from .brains import NNBrain
from .game import Grid, Snake


#######################################
#
# .-. .-. .-. .-. .-. . . .-. .-.
# `-. |-   |   |   |  |\| |.. `-.
# `-' `-'  '   '  `-' ' ` `-' `-'
#
#######################################


GRID_SIZE = 32

STARTING_SCORE = 0
STARTING_STEPS = 200

FOOD_SCORE = 150
FOOD_STEPS = 50

STEP_SCORE = 1

STEP_MOVES_COST = 1


###########################################################
#
# .d8888.  .o88b. db   db  .d88b.   .d88b.  db
# 88'  YP d8P  Y8 88   88 .8P  Y8. .8P  Y8. 88
# `8bo.   8P      88ooo88 88    88 88    88 88
#   `Y8b. 8b      88~~~88 88    88 88    88 88
# db   8D Y8b  d8 88   88 `8b  d8' `8b  d8' 88booo.
# `8888Y'  `Y88P' YP   YP  `Y88P'   `Y88P'  Y88888P
#
###########################################################


def run_one_game(snake):
    """Run one game from the beginning to the snake's death using arbitrary policy.

    returns: score gained by the snake
    """

    score = STARTING_SCORE
    moves = STARTING_STEPS

    while moves > 0 and snake.in_field() and not snake.self_destruction():
        if snake.on_food():
            score += FOOD_SCORE
            moves += FOOD_STEPS
        else:
            score += STEP_SCORE
        snake.use_brain()
        snake.move()
        moves -= STEP_MOVES_COST

    return score


def run_generation(gen):
    """Run one game on snake using every brain in generation.

    gen: list of valid brains

    returns: list of tuples (brain, score)
    """

    with_scores = []
    grid = Grid(GRID_SIZE)
    for brain in gen:
        score = run_one_game(Snake(grid, brain=brain))
        with_scores.append((brain, score))

    return with_scores


def make_new_generation(with_scores, chosen_ones, elite=0.05, rep=0.15, mut=0.3, mut_f=0.25):
    """Make new generation from with_scores by taking elite individuals and crossover old brains with mutation.

    with_scores: old generation with earned scores
    chosen_ones: dict-like object to save last and best brains with their scores
    elite: fraction of elite individuals to survive
    rep: fraction of brains taken to reproduction (including elite ones)
    mut: fraction of brains mutated (excluding elite ones)
    mut_f: average fraction of parameters changed in each mutated brain

    returns: new generation
    """

    with_scores.sort(key=lambda x: x[1], reverse=True)
    best_of_gen = with_scores[0]
    chosen_ones['last'] = best_of_gen
    chosen_ones['best'] = max(best_of_gen, chosen_ones['best'], key=lambda x: x[1])

    elite_end = int(len(with_scores) * elite) + 1
    rep_end = int(len(with_scores) * rep) + 1

    new_gen = [b[0] for b in with_scores[:elite_end]]
    rep_group = [b[0] for b in with_scores[:rep_end]]

    for _ in range(len(with_scores) - len(new_gen)):
        brain1 = random.choice(rep_group)
        brain2 = random.choice(rep_group)

        child = brain1.crossover(brain2)
        new_gen.append(child)

    for b in new_gen[-int(len(new_gen) * mut):]:
        b.mutate(mut_f)

    return new_gen


def run_age(gens, colony, name, load=False):
    """Run evolution algorithm and save results.

    gens: number of generations
    colony: number of snakes in every generation
    name: name of shelve to save results in
    load: whether to load existing generation saved in _name_ file
    """

    # dictionary for the best in the last generation and the best of all snakes ever born
    chosen_ones = {'last': (None, 0), 'best': (None, 0)}

    gen = []
    if load:
        with shelve.open(name, 'r') as db:
            chosen_ones = db['chosen']
            for b in db['gen']:
                gen.append(b[0])
    else:
        for _ in range(colony):
            gen.append(NNBrain())

    begin_time = time.time()
    for i in range(gens):
        with_scores = run_generation(gen)
        gen = make_new_generation(with_scores, chosen_ones)

        current_time = time.time()
        so_far = current_time - begin_time
        to_end = int((gens-(i+1))/(i+1) * so_far)
        hours, remainder = divmod(to_end, 3600)
        minutes, seconds = divmod(remainder, 60)
        hours, minutes, seconds = str(hours).zfill(2), str(minutes).zfill(2), str(seconds).zfill(2)

        # just to monitor progress
        print(f'\r{i + 1}/{gens}, estimated time amount: {hours}:{minutes}:{seconds}', end='')
    print('')

    last_gen_with_scores = sorted(run_generation(gen), key=lambda x: x[1], reverse=True)

    with shelve.open(name, 'c') as db:
        db['chosen'] = chosen_ones
        db['gen'] = last_gen_with_scores
        if load:
            db['num'] = db['num'] + gens
        else:
            db['num'] = gens


if __name__ == '__main__':
    run_age(200, 300, 'new_cave')
