import shelve

import pygame

from .game import Grid, Snake, Direction


#######################################
#
# ┌─┐┌─┐┌┐┌┌─┐┌┬┐┌─┐┌┐┌┌┬┐┌─┐
# │  │ ││││└─┐ │ ├─┤│││ │ └─┐
# └─┘└─┘┘└┘└─┘ ┴ ┴ ┴┘└┘ ┴ └─┘
#
#######################################


# main graphic numbers
WIDTH, HEIGHT = 720, 720
SIZE = 30
FONT_SIZE = 75
OFFSET = 3
fps = 12

# grid sizes
H = HEIGHT // SIZE
W = WIDTH // SIZE

# colors
BACKGROUND_COLOR = (50, 50, 50)
FOOD_COLOR = (0, 255, 0)
FONT_COLOR = (0, 255, 255)
SNAKE_COLOR = (255, 255, 255)
SNAKE_HEAD_COLOR = (252, 3, 0)


#######################################
# ___          _
#  | |_  _    (_| _ __  _
#  | | |(/_   __|(_||||(/_
#
#######################################


def interview(brain=None):
    """Main function to start a game.

    brain: Brain object to control snake, if None then only keyboard works
    """

    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    clock = pygame.time.Clock()

    grid = Grid(H, W, roll=False)
    snake = Snake(grid, brain=brain)
    run = True

    while run:
        move_flag = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT \
                    or (event.type == pygame.KEYDOWN and (event.key == pygame.K_ESCAPE or event.key == pygame.K_q)):
                break
            elif not move_flag and event.type == pygame.KEYDOWN:  # universal possession
                if event.key == pygame.K_RIGHT and snake.direction != Direction.LEFT:
                    snake.direction = Direction.RIGHT
                if event.key == pygame.K_UP and snake.direction != Direction.DOWN:
                    snake.direction = Direction.UP
                if event.key == pygame.K_LEFT and snake.direction != Direction.RIGHT:
                    snake.direction = Direction.LEFT
                if event.key == pygame.K_DOWN and snake.direction != Direction.UP:
                    snake.direction = Direction.DOWN
                move_flag = True
        if snake.brain:
            snake.use_brain()

        screen.fill(BACKGROUND_COLOR)

        for piece1, piece2 in zip(snake.body[:-2], snake.body[1:-1]):
            left = min(piece1[1], piece2[1])
            top = min(piece1[0], piece2[0])
            body_piece_rect = pygame.Rect(left * SIZE + OFFSET, top * SIZE + OFFSET,
                                          SIZE + (SIZE if piece1[0] == piece2[0] else 0) - 2 * OFFSET,
                                          SIZE + (SIZE if piece1[1] == piece2[1] else 0) - 2 * OFFSET)
            pygame.draw.rect(screen, SNAKE_COLOR, body_piece_rect)
        # tail drawn separately
        piece1, piece2 = snake.body[-2], snake.body[-1]
        if piece1 != piece2:
            left = min(piece1[1], piece2[1])
            top = min(piece1[0], piece2[0])
            body_piece_rect = pygame.Rect(left * SIZE + OFFSET, top * SIZE + OFFSET,
                                          SIZE + (SIZE if piece1[0] == piece2[0] else 0) - 2 * OFFSET,
                                          SIZE + (SIZE if piece1[1] == piece2[1] else 0) - 2 * OFFSET)
            pygame.draw.rect(screen, SNAKE_COLOR, body_piece_rect)

        food_y, food_x = snake.env.food
        food_rect = pygame.Rect(food_x * SIZE + OFFSET, food_y * SIZE + OFFSET,
                                SIZE - 2 * OFFSET, SIZE - 2 * OFFSET)
        pygame.draw.rect(screen, FOOD_COLOR, food_rect)
        head = snake.body[0]
        head_rect = pygame.Rect(head[1] * SIZE + OFFSET, head[0] * SIZE + OFFSET,
                                SIZE - 2 * OFFSET, SIZE - 2 * OFFSET)
        pygame.draw.rect(screen, SNAKE_HEAD_COLOR, head_rect)
        font = pygame.font.SysFont(None, FONT_SIZE)
        font_img = font.render(f'{snake.cells}', True, FONT_COLOR)
        screen.blit(font_img, (SIZE // 2, SIZE // 2))

        pygame.display.flip()

        grid.step()
        if not snake.in_field() or snake.self_destruction():
            run = False

        clock.tick(fps)

    pygame.quit()
